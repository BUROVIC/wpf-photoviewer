﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace WpfPhotoViewer
{
	/// <summary>
	/// Константы
	/// </summary>
	public static class Globals
	{
		/// <summary>
		/// Путь к xml файлу с комментариями изображений
		/// </summary>
		public static string ImageDataFile = "ImageData.xml";

		/// <summary>
		/// Получение хэша файла, принцип работы данного метода понимаю с трудом и написал его не сам
		/// </summary>
		public static string ComputeMd5Checksum(string path)
		{
			using (FileStream fs = File.OpenRead(path))
			{
				MD5 md5 = new MD5CryptoServiceProvider();
				byte[] fileData = new byte[fs.Length];
				fs.Read(fileData, 0, (int)fs.Length);
				byte[] checkSum = md5.ComputeHash(fileData);
				string result = BitConverter.ToString(checkSum).Replace("-", String.Empty);
				return result;
			}
		}
	}
}
