﻿namespace WpfPhotoViewer.Data
{
	/// <summary>
	/// Сведения об изображении
	/// </summary>
	public class ImageData
	{
		public ImageData(string imagePath, string name, int fileSize, ImageCommentsXml comments)
		{
			ImagePath = imagePath;
			Name = name;
			FileSize = fileSize / 1024;
			Hash = Globals.ComputeMd5Checksum(ImagePath);
			Comment = comments.LoadComment(Hash);
		}

		/// <summary>
		/// Путь к данному изображению
		/// </summary>
		public string ImagePath { get; private set; }

		/// <summary>
		/// Наименование изображения вместе с типом файла
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Вес изображения (kb)
		/// </summary>
		public int FileSize { get; private set; }

		/// <summary>
		/// Комментарий к изображению
		/// </summary>
		public string Comment { get; set; }

		/// <summary>
		/// Хэш изображения
		/// </summary>
		public string Hash { get; private set; }
	}
}
