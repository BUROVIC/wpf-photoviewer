﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;

namespace WpfPhotoViewer.Data
{
	/// <summary>
	/// Самосериализуемый и самодесериализуемый класс, через него мы сохраняем и подгружаем комментарии, сооответствующие хэшу изображения
	/// </summary>
	public class ImageCommentsXml
	{
		/// <summary>
		/// Лист хэша
		/// </summary>
		public List<string> HashList;

		/// <summary>
		/// Лист комментариев, каждый из которых соответствует хэшу с таким же номером в ListHash
		/// </summary>
		public List<string> Comments;

		/// <summary>
		/// Десериализуем все поля данного класса (HashList, Comments)
		/// </summary>
		public static ImageCommentsXml GetComments()
		{
			ImageCommentsXml text;
			var filename = Globals.ImageDataFile;

			if (File.Exists(filename))
			{
				using (var fs = new FileStream(filename, FileMode.Open))
				{
					var xser = new XmlSerializer(typeof(ImageCommentsXml));
					text = (ImageCommentsXml)xser.Deserialize(fs);
					fs.Close();
				}
			}
			else text = new ImageCommentsXml();

			return text;
		}

		/// <summary>
		/// Сериализует все поля данного класса
		/// Если HashList и Comments null, создаёт новые пустые листы (такой случай возникает после первой попытки сериализации)
		/// </summary>
		/// <param name="dataList"></param>
		public void Save(ObservableCollection<ImageData> dataList)
		{
			if (dataList == null)
			{
				dataList = new ObservableCollection<ImageData>();
			}
			if (HashList == null)
			{
				HashList = new List<string>();
				Comments = new List<string>();
			}
			SaveComments(dataList);
			var filename = Globals.ImageDataFile;

			if (File.Exists(filename)) File.Delete(filename);


			using (var fs = new FileStream(filename, FileMode.Create))
			{
				var xser = new XmlSerializer(typeof(ImageCommentsXml));
				xser.Serialize(fs, this);
				fs.Close();
			}
		}

		/// <summary>
		/// Возвращает комментарий, соответствующий переданному хэшу
		/// </summary>
		public string LoadComment(string hash)
		{
			var i = 0;
			if (HashList != null)
			{
				foreach (var h in HashList)
				{
					if (h == hash)
					{
						return Comments[i];
					}
					i++;
				}
				return null;
			}
			return null;
		}

		/// <summary>
		/// Переносит данные из каждого элемента типа ImageData в листы HashList и Comments
		/// </summary>
		private void SaveComments(ObservableCollection<ImageData> dataList)
		{
			var oldComments = new List<string>();
			foreach (var comment in Comments)
			{
				oldComments.Add(comment);
			}
			foreach (var data in dataList)
			{
				var inHashList = false;
				var i = 0;
				foreach (var h in HashList)
				{
					if (data.Hash == h)
					{
						inHashList = true;
						if (oldComments[i] != data.Comment)
						{
							Comments[i] = data.Comment;							
							break;
						}
					}
					i ++;
				}
				if (data.Comment != null && !inHashList)
				{
					Comments.Add(data.Comment);
					oldComments.Add(null);
					HashList.Add(data.Hash);					
				}
			}
		}
	}
}
