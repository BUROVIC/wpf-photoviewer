﻿using System;
using System.Windows.Input;

namespace WpfPhotoViewer
{
	/// <summary>
	/// Класс поддержки работы с командами
	/// </summary>
	public class CommandHandler : ICommand
	{
		public CommandHandler(Action action, bool canExecute)
		{
			this.action = action;
			this.canExecute = canExecute;
		}

		/// <summary>
		/// Действие
		/// </summary>
		private Action action;

		/// <summary>
		/// Может ли она быть выполнена
		/// </summary>
		private bool canExecute;

		/// <summary>
		/// Defines the method that determines whether the command can execute in its current state.
		/// </summary>
		/// <returns>
		/// true if this command can be executed; otherwise, false.
		/// </returns>
		/// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
		public bool CanExecute(object parameter)
		{
			return canExecute;
		}

		/// <summary>
		/// Occurs when changes occur that affect whether or not the command should execute.
		/// </summary>
		public event EventHandler CanExecuteChanged;

		/// <summary>
		/// Defines the method to be called when the command is invoked.
		/// </summary>
		/// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
		public void Execute(object parameter)
		{
			action();
		}
	}
}
