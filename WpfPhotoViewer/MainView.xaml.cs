﻿using System;
using System.Windows;

namespace WpfPhotoViewer
{
	/// <summary>
	/// Interaction logic for MainWiew.xaml
	/// Данный класс по паттерну MVVM вроде бы должен оставаться пустым, но мне было необходимо привязать выполнение метода из MainViewModel к закрытию программы
	/// Иного способа я не нашёл
	/// </summary>
	public partial class MainView : Window
	{
		public MainView()
		{
			InitializeComponent();
			Closed += MainView_Closed;
			mainViewModel = new MainViewModel();
			DataContext = mainViewModel;
		}

		/// <summary>
		/// mainViewModel
		/// </summary>
		private MainViewModel mainViewModel;

		/// <summary>
		/// Сериализуем комментарии по закрытию приложения
		/// </summary>
		private void MainView_Closed(object sender, EventArgs e)
		{
			mainViewModel.SaveImageData();
		}
	}
}
