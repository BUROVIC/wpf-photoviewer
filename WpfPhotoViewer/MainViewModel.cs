﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WpfPhotoViewer.Data;

namespace WpfPhotoViewer
{
	/// <summary>
	/// MainViewModel, связанный с MainView по паттерну MVVM
	/// </summary>
	public class MainViewModel : INotifyPropertyChanged
	{		
		public MainViewModel()
		{
			Path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			imageComments = ImageCommentsXml.GetComments();
		}		

		/// <summary>
		/// Хранит в себе соответствующие друг другу комментарии и хэш изображений, а также методы по их серриализации и десерриализации
		/// </summary>
		private ImageCommentsXml imageComments;

		/// <summary>
		/// Путь к диретории
		/// </summary>
		public string Path { get; set; }

		/// <summary>
		/// Фильтр текста (со строгим регистром)
		/// </summary>
		public string Filter { get; set; }

		/// <summary>
		/// Видимость индикатора поиска
		/// </summary>
		private Visibility searchIndicatorVisibility = Visibility.Hidden;

		/// <summary>
		/// Видимость индикатора поиска
		/// get просто возвращает searchIndicatorVisibility
		/// set задаёт searchIndicatorVisibility переданное значение и оповещает UI об изменившемся свойстве
		/// </summary>
		public Visibility SearchIndicatorVisibility
		{
			get { return searchIndicatorVisibility; }
			set
			{
				searchIndicatorVisibility = value;
				NotifyPropertyChanged();
			}
		}

		/// <summary>
		/// Поддерживаемые типы файлов, наверное стоило вынести это в Globals...
		/// </summary>
		private static readonly List<string> FileTypes = new List<string>{".png", ".jpg"};

		/// <summary>
		/// Сохраняем комментарии предыдущей директории, если метод получает true, и грузим новую директорию в асинхронном процессе (UI апдейтится)
		/// </summary>
		/// <param name="withSave"></param>
		public void GetImageData(bool withSave)
		{
			if (SearchIndicatorVisibility != Visibility.Visible)
			{
				if (withSave)
				{
					imageComments.Save(ListImageData);
				}
				SearchIndicatorVisibility = Visibility.Visible;
				var recursiveImageSearchTask = CreateRecursiveImageSearchTask(Path);
				recursiveImageSearchTask.ContinueWith(task =>
				{
					var recursiveImageSearchResult = recursiveImageSearchTask.Result;
					ListImageData = new ObservableCollection<ImageData>(recursiveImageSearchResult);
					imageComments = ImageCommentsXml.GetComments();
					SearchIndicatorVisibility = Visibility.Hidden;
				});
				recursiveImageSearchTask.Start();
			}
		}

		/// <summary>
		/// В отдельном процессе получаем лист файлов по переданному пути
		/// Лишь благодаря моему дяде это происходит в асинхронном процессе, сам я к этому прийти не смог)
		/// </summary>
		private Task<List<ImageData>> CreateRecursiveImageSearchTask(string path)
		{
			return new Task<List<ImageData>>(() =>
			{				
				var dataList = new List<ImageData>();
				RecursiveImageSearch(path, dataList);
				return dataList;
			});
		}

		/// <summary>
		/// Рекурсивно (из папок, подпапок и т.д.) заполняем файлами переданную коллекцию по переданному пути
		/// </summary>
		private void RecursiveImageSearch(string path, List<ImageData> dataList)
		{
			string[] filePaths;
			try
			{
				filePaths = Directory.GetFiles(path);
			}
			catch (Exception)
			{
				filePaths = new string[0];
			}
			var pathCount = path.Length;
			foreach (var filePath in filePaths)
			{
				foreach (var fileType in FileTypes)
				{
					FileInfo fileInfo;
					try
					{
						fileInfo = new FileInfo(filePath);
					}
					catch (Exception)
					{
						continue;
					}
					if (filePath.EndsWith(fileType))
					{
						var name = filePath.Remove(0, pathCount + 1);
						if (Filter != null)
						{
							if (name.Contains(Filter))
							{
								try
								{
									dataList.Add(new ImageData(filePath, name, (int)fileInfo.Length, imageComments));
								}
								catch (Exception)
								{
									//"Файл не был найден". Это весьма странный эксепшн, но он возникал у меня с файлами, находящимися в корзине, на данном моменте
								}	
							}
						}
						else
						{
							try
							{
								dataList.Add(new ImageData(filePath, name, (int)fileInfo.Length, imageComments));
							}
							catch (Exception)
							{								
								//"Файл не был найден"
							}							
						}
					}
				}
			}
			string[] directories;
			try
			{
				directories = Directory.GetDirectories(path);
			}
			catch (Exception)
			{
				directories = new string[0];
			}
			foreach (var directory in directories)
			{
				RecursiveImageSearch(directory, dataList);
			}
		}

		/// <summary>
		/// Команда запуска поиска
		/// </summary>
		private ICommand startSearchCommand;

		/// <summary>
		/// Команда запуска поиска
		/// </summary>
		public ICommand StartSearchCommand
		{
			get
			{
				//Возвращает startSearchCommand, если оно null, присваивает ему CommandHandler (и возвращает)
				return startSearchCommand ?? (startSearchCommand = new CommandHandler(() => GetImageData(true), true));
			}
		}

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;
		
		/// <summary>
		/// Нотифицирует об изменении свойства по его имени
		/// </summary>
		private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		/// <summary>
		/// Список сведений об изображениях
		/// </summary>
		private ObservableCollection<ImageData> listImageData;

		/// <summary>
		/// Список сведений об изображениях
		/// </summary>
		public ObservableCollection<ImageData> ListImageData
		{			
			get
			{
				//Возвращает listImageData (если он null, запускает метод GetImageData, в котором данная коллекция создаётся
				if (listImageData == null)
				{
					GetImageData(false);
				}
				return listImageData;
			}
			set
			{
				//Присваивает значение и оповещает интерфейс о его изменении
				if (listImageData != value)
				{
					listImageData = value;
					NotifyPropertyChanged();
				}				
			}
		}

		/// <summary>
		/// Сохраняем комментарии к изображениям
		/// </summary>
		public void SaveImageData()
		{
			imageComments.Save(ListImageData);
		}
	}
}
